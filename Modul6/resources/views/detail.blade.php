@extends('layouts.apppost')

@section('content')
@foreach($posts as $data)
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-11" style="margin-bottom:20px;">
        <nav>
        <ul style="list-style:none;padding: 0 10px;display:inline-table;position: relative;">
        <li style="float:left;padding:20px;poistion:relative;">
            <div style="margin-bottom:20px;margin-top:20px;">
            <img src="{{ $data->image }}" style="width: 100%; height:100%;">
            </div>
        </li>
        <li style="float:left;padding:20px;poistion:relative;">
            <div style="margin-bottom:20px;margin-top:20px;width:200%;">
            
            <img src ="/img/{{ $user->avatar }}" style="width:40px;height:40px;border-radius:50%;margin-right:20px;">
            <a style="font-size:25px;">{{ Auth::user()->name }}</a>
            <hr>
            @foreach($commentar as $cm)
                {{ $user->name}}: {{$cm['comment']}}<br>
                @endforeach
            <hr>
            <a class="fa fa-heart-o" href="{{ route('postlike',['id'=>$data->id]) }}"></a>
            <i class="fa fa-comment-o" style="margin-left:10px;"> </i>
            <b>{{$data->likes}} Likes</b>
            <br>
            
            <br>
            <form>
            <input type="text" class="form-control" style="width:100%" placeholder="Add a comment..">
            </form>
            </div>
        </li>
        </ul>
        </nav>
        </div>
    </div>
</div>
@endforeach
@endsection
