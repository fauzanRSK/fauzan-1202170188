@extends('layouts.appedit2')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="margin-bottom:20px;">
            <h1>Edit Profile</h1>
            <form enctype="multipart/form-data" action="{{route('Update_profile',$user->id)}}" method="POST">
            @method('POST')
                <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <label>Title</label>
                <input class="form-control" placeholder="{{ Auth::user()->title }}" name="title">
                <br>
                <label>Description</label>
                <input class="form-control" placeholder="{{ Auth::user()->description }}" name="description">
                <br>
                <label>URL</label>
                <input class="form-control" placeholder="{{ Auth::user()->url }}" name="url">
                <br>
                <label>Profile Image</label>
                <input type="file" class="form-control-file" id="exampleFormContolFile1" name="Avatar">  
                <br>
                <input type="submit" class="btn btn-primary" value="Submit">
            </form>
        </div>
    </div>
</div>
@endsection
