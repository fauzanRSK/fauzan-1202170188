@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
        @foreach($posts as $data)
            <div class="card"  style="margin-bottom:20px;">
                <div class="card-header">
                <img src =" /img/{{ $user->avatar }}" style="width:50px;height:50px;border-radius:50%;border-color:blue;">
                {{ Auth::user()->name }}
                </div>

                <div class="card-body">
                <a href="{{ route('limit') }}"><img src="{{ $data->image }}" style="width: 100%; height:100%;"></a>
                </div>
                <div class="card-footer">
                <a class="fa fa-heart-o" href="{{ route('postlike',['id'=>$data->id]) }}"></a>
                <i class="fa fa-comment-o" style="margin-left:10px;"></i>
                <b style="margin-left:10px;">
                {{$data->likes}} Likes
                </b>
                <br>
                <b>{{ Auth::user()->email }}</b>
                <hr>
                @foreach($commentar as $cm)
                {{ $user->name}}: {{$cm['comment']}}<br>
                @endforeach
                <table>
                <form action="{{route('comment')}}">
                    <tr>
                        <td style="width:100%;"><input type="text" class="form-control" style="width:100%" placeholder="Comment"></td>
                        <input type="hidden" class="form-control" name="id_user" value="{{Auth::user()->id}}">
                        <input type="hidden" class="form-control" name="id_post" value="{{$data->id}}">
                        <td><button type="submit" class="btn btn-primary">Post</button></td>
                    </tr>
                </form>
                </table>
                </div>
            </div>
            @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
