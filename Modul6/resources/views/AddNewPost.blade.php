@extends('layouts.apppost')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="margin-bottom:20px;">
            <h1>Add New Post</h1>
            <br>
            <form action="{{route('addNewPost')}}" enctype="multipart/form-data" method="POST">
                <label>Post Caption</label>
                <input type="text" class="form-control" name="caption">
                <br>
                <label>Profile Image</label>
                <input type="file" class="form-control-file" id="exampleFormContolFile1" name="photo">
                <br>
                <button type="submit" class="btn btn-primary">Add New Post</button>
            </form>
        </div>
    </div>
</div>
@endsection
