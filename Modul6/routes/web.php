<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/post/like/{id}', 'PostController@like')->name('postlike');
Route::get('/post/likelike/{id}', 'PostController@likelike')->name('postlikelike');
Route::get('/post/commentar/', 'PostsController@comment')->name('comment');
Route::post('/post','PostController@post')->name('addNewPost');

Route::get('profile','ProfileController@profile')->name('profile');
Route::get('EditProfile','ProfileController@EditProfile');
Route::post('Update_profile','ProfileController@Update_profile')->name('Update_profile');
Route::get('detail','PostController@detail')->name('limit');
Route::get('AddNewPost','PostController@addnp')->name('addPost');





