<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class commentar extends Model
{
    protected $table="komentar_posts";
    protected $fillable=['id', 'id_user', 'id_post', 'comment', 'created_at', 'updated_at'];

 public function user(){
        return $this->belongsTo('App\user','id_user');
    }
public function posts(){
        return $this->belongsTo('App\posts','is_post');
    }

}

   

    