<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posts;
use App\commentar;
use Auth;

class PostController extends Controller
{
    

    public function like($id){
        posts::find($id)->increment('likes');
        return redirect('home'); 

    }
    public function likelike($id){
        posts::find($id)->increment('likes');
        return redirect('detail'); 

    }
    public function detail(){
        $posts = posts::get();
        $commentar = commentar::get();
        return view('detail',['posts' => $posts , 'commentar' => $commentar],array('user' => Auth::user()));
    }
    public function addnp(){
        return view('AddNewPost', array('user' => Auth::user()));
    }
    public function comment(Request $request){
        $comment = new commentar();
        $comment->id_user = $request->id_user;
        $comment->id_post = $request->id_post;
        $comment->commentar=$request->komen;
        $comment->save();
        return redirect('home');
    }

    public function post(){
        $post = request()->validate([
			'caption' => 'required',
			'photo' => ['required', 'photo'],
		]);

		$Path = request('photo')->post('uploads', 'public');

		$photo = Image::make(public_path("img/{$Path}"))->fit(1200, 1200);
		$photo->save();

		auth()->user()->posts()->create([
			'caption' => $post['caption'],
			'photo' => $Path,
			'likes' => 0,
		]);

        return redirect()->route('home')->with('succes','Data Telah Masuk');
    }
}
