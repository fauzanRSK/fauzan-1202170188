<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use illuminate\Support\Facades\DB;
use App\posts;
use App\User;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        $posts = posts::get();
        return view('profil', ['posts' => $posts]);
    }
    
    public function editprofile($id){

        $posts = User::find($id);        
        return view('profil', ['posts' => $posts]);
    }

}
