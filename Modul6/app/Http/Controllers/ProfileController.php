<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;

class ProfileController extends Controller
{
    //
    public function profile(){
        return view('profile', array('user' => Auth::user()));
    }
    public function EditProfile(){
        return view('EditProfile', array('user' => Auth::user()));
    }
    public function Update_profile(Request $request, $id){
        
        // Mengurus user edit profile
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required',
            'avatar' => 'required'
        ]);

            $this->authorize('modifyUser',auth()->user());
            $user->title = $request->input('title');
            $user->description = $request->input('description');
            $user->url = $request->input('url');
            $avatar = $request->file('avatar');
            if(!$file){
                return redirect()->route('Profile')->with('alert','Data Harus Terisi');
            }
            $avatar_file = $avatar->getClientOriginalName();
            $path = public_path("/img");
            $avatar->move($path, $avatar_file);
            $user->avatar = $avatar_file;
            $user->save();

            return redirect('Profile')->with('succes' ,'Post Edited');
    }
}
