<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\posts;
use Auth;
use App\commentar;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = posts::get();
        $commentar = commentar::get();
        return view('home',['posts' => $posts , 'commentar' => $commentar],array('user' => Auth::user()));
    }
}
