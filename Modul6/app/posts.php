<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class posts extends Model
{
    protected $table="posts";
    protected $fillable=['id', 'id_user', 'caption', 'image', 'like', 'created_at', 'updated_at'];

    public function commentar(){
        return $this->hasMany( 'App\commentar','id_post');
    }

    public function user(){
        return $this->belongsTo( 'App\User','id_user');
    }

    

}
