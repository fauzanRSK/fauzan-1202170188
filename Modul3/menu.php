<!DOCTYPE html>
<?php
$nama=$_POST['nama'];
$tel=$_POST['NoT'];
$date = isset($_POST['datepicker']) ? $_POST['datepicker'] : (new DateTime)->format("d M Y");
$driver=$_POST['jd'];
$BK=$_POST['bk']??"";
if($BK == ""){
    $BK="Tidak";
}else{
    $BK="Ya";
}
?>

<html lang="en">
<head>
    <title>Document</title>
    <style>
        table{
            position: relative;
            width: 100%;
            padding: 10px;
            margin: 15px;
            text-align: center;
        }
        table td{
            position: relative;
        }
        table input{
            position: relative;
            padding: 5px;
            border-radius: 5px;
        }
        .areao{
            position: relative;
            padding-top: 0px;
            padding-bottom: 50px;
            padding-left: 15px;
            padding-right: 15px;
            min-width: 0px;
            height: 100px;
            text-align: left;
        }
        .dataOjol{
            position: relative;
            width: 400px;
            height: 600px;
            left: 10%;
            top: 10%;
            text-align:center;
        }
        .dataMenu{
            position: relative;
            width: 400px;
            height: 600px;
            left: 60%;
            top: -42%;
            text-align:center;
            overflow:auto;
        }
        .data{
            position: relative;
            width: 100%;
            height: 1500px;
            margin-bottom: 10px;
        }
        .area{
            position: relative;
            width: 100%;
        }
        .BS{
            text-align:center;
            border-radius: 5px;
            background-color: purple;
            border: none;
            color: white;
            position: relative;
            height: 40px;
            width: 100%;
        }
        .BSK{
            text-align:center;
            border-radius: 5px;
            background-color: purple;
            border: none;
            color: white;
            position: relative;
            height: 40px;
            width: 70%;
        }
    </style>
</head>
<body>
    <div class="data">
        
        <form action="form.html" method="POST">
        <div class="dataOjol">
        <h1>
            <?php
                echo "~Data Driver Ojol~"
            ?>
        </h1>
    <label><b>Nama</b></label> 
    <p><?= $nama ?></p> 
    <br>
    <label><b>Nomor Telepon</b></label>
     <p><?= $tel ?></p>
     <br>
     <label><b>Tanggal</b></label>
     <p><?= $date ?></p>
     <br> 
     <label><b>Asal Driver</b></label> 
     <p><?= $driver ?></p>
     <br> 
     <label><b>Bawa Kantong</b></label> 
     <p><?= $BK ?></p>
     <br>
        <div>
            <a href="form.html"><button name="kembali" type="button" class="BSK"  onclick="check()">&lt;&lt;KEMBALI</button></a>
        
        
        </div>
        </div>
        </form>
        <div class="dataMenu">
            <h1>
            <?php
             echo "~Menu~"
            ?>
            </h1>
            <p>
            <?php
                echo "pilih menu"
            ?>
            </p>
            <div class="areao">
            <div class="area">
            <form action="nota.php" method="POST">
                    <table>
                            <th>
                                        <td><input type="checkbox" name="k[]" value="ECS" ><label>Es Coklat Susu</label></td>
                                        <td ><label style="color:gray;">Rp.28.000,-</label></td>
                                    </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                <table>
                            <th>
                                        <td><input type="checkbox" name="k[]" value="ESMa" ><label>Es Susu Matcha</label></td>
                                        <td><label style="color:gray;">Rp.18.000,-</label></td>
                                    </th>
                            </table>
            </div>
            <hr>
            <div class="area">
            <table>
                            <th>
                                        <td><input type="checkbox" name="k[]" value="ESMo" ><label>Es Susu Mojicha</label></td>
                                        <td><label style="color:gray;">Rp.15.000,-</label></td>
                                    </th>
                            </table>
            </div>
            <hr>
            <div class="area">
            <table>
                            <th>
                                        <td><input type="checkbox" name="k[]" value="EML" ><label>Es Matcha Latte</label></td>
                                        <td><label style="color:gray;">Rp.30.000,-</label></td>
                                    </th>
                            </table>
            </div>
            <hr>
            <div class="area">
            <table>
                            <th>
                                        <td><input type="checkbox" name="k[]" value="ETS" ><label>Es Taro Susu</label></td>
                                        <td><label style="color:gray;">Rp.21.000,-</label></td>
                                    </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td><label>Nomor Order</label></td>
                                        <td><label></label></td>
                                        <td><input type="number" name="nomerO" required=""></td>
                                </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td><label style="text-align:left;">Nama<br>Pemesan</label></td>
                                        <td><label></label></td>
                                        <td><input type="text" style="left:10px;" name="namap" required=""></td>
                                </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td><label>Email</label></td>
                                        <td><label></label></td>
                                        <td><input type="email" style="left:20px;" name="email" required=""></td>
                                </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td><label>Alamat Order</label></td>
                                        <td><label></label></td>
                                        <td><input type="text" style="left:-5px;" name="AO" required=""></td>
                                </th>
                            </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td style="left: -10px;"><label>Member</label></td>
                                        <td><label></label></td>
                                        <td style="left:-25px;"><input type="radio" name="member" value="Ya" required><label>Ya</label>
                                        <input type="radio" name="member" value="Tidak" style="margin-left:30px;" required><label>Tidak</label></td>
                                </th>
                        </table>
            </div>
            <hr>
            <div class="area">
                    <table>
                                <th>
                                        <td><label>Metode<br>Pembayaran</label></td>
                                        <td><label></label></td>
                                        <td><select class="pick" name="mtp" required>
                                            <option selected="true" disabled="true">----Pilih Metode Pembayaran---</option>
                                            <option value="Cash">Cash<option>
                                            <option value="E-Money">E-Money<option>
                                            <option value="CC">Credit Card<option>
                                            <option value="Lainnya">lainnya<option>    
                                        </select></td>
                                </th>
                            </table>
            </div>
            <hr>
                <input name="submitButton" type="submit" class="BS" value="SEND">
            </form>
            </div>
        </div>
        
    </div>
</body>
</html>