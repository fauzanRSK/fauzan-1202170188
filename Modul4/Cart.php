<?php
include "connect.php";
include "session.php";
$query = "SELECT * FROM cart";
$success = mysqli_query($koneksi,$query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Profil</title>
    <style>
    .pic{
        width: 150px;
        height: 50px;
    }
    .add{
        position: relative;
        margin-left: 20%;
        margin-right: 62%;
        left: 60%;
        top: -60px;
    }
    .size{
        height: 65px;
    }
    .button{
        background-color: white;
        position: relative;
        top: 6px;
        color: black;
        padding: 16px;
        font-size: 16px;
        transition-duration: .5s;
        border: none;
    }
    .button:hover{
            color: gray;
            border: 1px crimson;
            cursor: pointer;
    }
    
    .dropbtn {
        background-color: white;
        color: black;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }
        .dropbtn:hover, .dropbtn:focus {
        background-color: ;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        top: -50px;
        left: 30%;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        overflow: auto;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}
    table{
        position: relative;
        border: 2;
        border-color:black;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-left: -20px;
        margin-bottom:25%;
    }
    table tr{
        border:1;
    }
    table tr td{
        position: relative;
        border: 1;
        border-color: black;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 100px;
    }
    .delete {
        background-color: red;
        color: white;
        border: none;
        border-radius:2px;
    }
    </style>
</head>
<body>
<div class="size">
        <img src="EAD.png" class="pic">
        <div class="add">
            <form action="Cart.php" method="POST">
            <button class="button"><img src="Shop.png" width="20px" height="20px"></button>
            </form>
            <div class="dropdown">
                <button class="dropbtn" onclick="drop()"><?php echo "$login_session2" ?>  <img src="Segitiga.png" width="10px" height="10px"></button>
                <div id="myDropdown" class = "dropdown-content">
                    <a href="Update Profile.php">Edit Profile</a>
                    <a href="Logout.php">Log Out</a>
                </div>
            </div>
        </div>  
    </div>
    <hr>
    <center>
    <table id="cart">
        <tr>
            <td>No</td>
            <td>Product</td>
            <td colspan="2">Price</td>
        </tr>
        <?php
            while($hasil = mysqli_fetch_array($success)){
            ?>
            <tr>
                <form action="Delete.php" method="POST">
                <td> <label name="ID"><?= $hasil['id']?></label> </td>
                <?php 
                $tes = $hasil['id'];
                echo "<input type='text' name='ID' value='$tes' hidden>";
                ?>
                <td> <?= $hasil['product']?></td>
                <td> <label name="pr" id="pr" onchange="total()"><?= $hasil['price']?></label></td>
                <td><button type="submit" name="delete" class="delete" >&times;</button></td>
                </form>
            </tr>
            <?php 
                $tot = null;
                $tot += $hasil['price'];
            } 
            $total = $tot; 
            ?>        
        <tr>
                <td  colspan="2" style="text-align:center;"><b>TOTAL</b></td>
                <td colspan="2"><label id="total"><?php echo "$total" ?></label></td>
        </tr>
    </table>
    </center>
    <p id="demo"> </p>
    <center>
        <p>@EAD Strore</p>
    </center>
    <script>
        function drop() {
          document.getElementById("myDropdown").classList.toggle("show");
        }
        
        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }
        </script>
</body>
</html>