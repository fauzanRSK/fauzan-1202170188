<?php
include "connect.php";
session_start();
$error = '';
if (isset($_POST['login'])){
        $email = $_POST['emaill'];
        $pass = $_POST['passl'];

        $query = "SELECT email, password FROM users where email=? AND password=? LIMIT 1";

        $stmt = $koneksi->prepare($query);
        $stmt->bind_param("ss",$email, $pass);
        $stmt->execute();
        $stmt->bind_result($email, $pass);
        $stmt->store_result();
        if($stmt->fetch()){
            $_SESSION['login_user'] = $email;
            echo "<script>alert('Login succeeded');</script>";
            header("location:HomeLogin.php");
        }else{
            echo "<script>alert('Email or Password is invalid');</script>";
            header("location:Home.php");
        }
        mysqli_close($koneksi);
}
?>