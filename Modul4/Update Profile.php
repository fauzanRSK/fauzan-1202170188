<?php
include "session.php";
if(!isset($_SESSION['login_user'])){
    header("location: Home.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit Profil</title>
    <style>
    .button{
        background-color: white;
        position: relative;
        top: 6px;
        color: black;
        padding: 16px;
        font-size: 16px;
        transition-duration: .5s;
        border: none;
    }
    .button:hover{
            color: gray;
            border: 1px crimson;
            cursor: pointer;
    }
    .pic{
        width: 150px;
        height: 50px;
    }
    .add{
        position: relative;
        margin-left: 20%;
        margin-right: 62%;
        left: 60%;
        top: -60px;
    }
    .size{
        height: 65px;
    }
    .dropbtn {
        background-color: white;
        color: black;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }
        .dropbtn:hover, .dropbtn:focus {
        background-color: white;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        top: -50px;
        left: 30%;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        overflow: auto;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}
    .profil{
        margin-top:20px;
        margin-left: 20%;
        margin-right: 20%;
        height: 500px;
        position:relative;
    }
    table{
        position: relative;
        border: 1;
        border-color: 10px black;
        padding-top: 10px;
        padding-bottom: 10px;
        margin-left: -20px;
        left:10%;
    }
    table tr{
    }
    table tr td{
        position: relative;
        border: 1;
        border-color: 10px black;
        padding-top: 10px;
        padding-bottom: 10px;
        padding-left: 100px;
    }
    table input{
        width: 250%;
    }
    .save{
        color:bisque;
        background-color: blue;
        border: none;
        border-radius: 2px;
        width: 550%;
        height: 25px;
        margin-top: 5px;
        margin-bottom: 5px;
        padding-bottom: 5px;
    }
    .cancel{
        background-color: white;
        color:blue;
        border: none;
        border-radius: 2px;
        width: 550%;
        height: 25px;
        margin-top: 5px;
        margin-bottom: 5px;
        padding-bottom: 5px;
    }
    </style>
</head>
<body>
<div class="size">
        <img src="EAD.png" class="pic">
        <div class="add">
            <form action="Cart.php" method="POST">
            <button class="button"><img src="Shop.png" width="20px" height="20px"></button>
            </form>
            <div class="dropdown">
                <button class="dropbtn" onclick="drop()"><?php echo "$login_session2" ?>  <img src="Segitiga.png" width="10px" height="10px"></button>
                <div id="myDropdown" class = "dropdown-content">
                    <a href="Update Profile.php">Edit Profile</a>
                    <a href="Logout.php">Log Out</a>
                </div>
            </div>
        </div>  
    </div>
    <hr>
    <br>
    <div class="profil">
        <h1 style="text-align: center;">Profile</h1>
        <form method="POST" action="EditProfil.php">
            <table>
                <tr>
                    <td><label>Email</label></td>
                    <td><label name="email"><?php echo "$login_session"?></label></td>
                </tr>
                <tr>
                    <td><label>Username</label></td>
                    <td><input type="text" placeholder="<?php echo "$login_session2" ?>" name="username"></td>
                </tr>
                <tr>
                    <td><label>Mobile Number</label></td>
                    <td><input type="number" placeholder="Mobile number" name="MN"></td>
                </tr>
                <tr>
                    <td><label>New Password</label></td>
                    <td><input type="password" placeholder="New Password" name="password"></td>
                </tr>
                <tr>
                    <td><label>Confirm Password</label></td>
                    <td><input type="password" placeholder="Confirm Password" name="Cpassword"></td>
                </tr>
                <tr>
                    <td><input type="submit" value="save" class="save" name="save"><br>
                    <button type="reset" value="cancel" class="cancel" name="save" onclick="back()">cancel</button></td>
                </tr>
            </table>
        </form>
    </div>
    <center>
        <p>@EAD Strore</p>
    </center>
    <script>
        function drop() {
          document.getElementById("myDropdown").classList.toggle("show");
        }
        
        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }
        function back(){
            window.location.href='HomeLogin.php';
        }

        </script>
</body>
</html>