<?php
include "session.php";

if(!isset($_SESSION['login_user'])){
    header("location: Home.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <style>
        nav{
            margin:auto;
            text-align: center;
            height: 100%;
            width: 100%;
            font-family: arial;
        }
        nav ul {
            list-style:none;
            padding: 0 10px;
            width: 100%;
            height: 250px;
            display: inline-table;
            position: relative;

        }
        nav ul li{
            float: left;
            padding:  20px;
            position: relative;
            left:15%;
        }
        .nav{
            background-color:rgb(240,240,240);
            height: 500px;
            width: 300px;
            text-align: left;
            border: 10px;
            border-color: black;
        }
    .pic{
        width: 150px;
        height: 50px;
    }
    .pic2{
        width: 300px;
        height: 250px;
    }
    .add{
        position: relative;
        margin-left: 20%;
        margin-right: 62%;
        left: 60%;
        top: -60px;
    }
    .size{
        height: 65px;
    }
    .button{
        background-color: white;
        position: relative;
        top: 6px;
        color: black;
        padding: 16px;
        font-size: 16px;
        transition-duration: .5s;
        border: none;
    }
    .button:hover{
            color: gray;
            border: 1px crimson;
            cursor: pointer;
    }
    .HC{
        height: 200px;
        margin-left: 10%;
        margin-right: 10%;
        background-repeat:repeat-x;
        background: rgb(0,215,255);
        background: linear-gradient(90deg, rgba(0,215,255,0.35) 0%, rgba(0,236,255,0.4962359943977591) 25%, rgba(0,227,255,0.35) 50%, rgba(0,254,255,0.5) 75%, rgba(0,255,239,0.35) 100%);
    }
    .buy{
        background-color:#008eff;
        color: white;
        border: none;
        border-radius: 5px;
        padding: 10px;
        font-size: 16px;
        transition-duration: .5s;
        margin-top: 20px; 
        width: 250px;
    }
    .buy:hover{
        background-color: linear-gradient(90deg, rgba(240,240,240,0.35) 0%, rgba(0,142,255,1) 100%);
        cursor:pointer;
    }
    .dropbtn {
        background-color: white;
        color: black;
        padding: 16px;
        font-size: 16px;
        border: none;
        cursor: pointer;
    }
        .dropbtn:hover, .dropbtn:focus {
        background-color: ;
    }

    .dropdown {
        position: relative;
        display: inline-block;
        top: -50px;
        left: 30%;
    }

    .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        overflow: auto;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }

    .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
    }

    .dropdown a:hover {background-color: #ddd;}

    .show {display: block;}
    </style>
</head>
<body>
    <div class="size">
        <img src="EAD.png" class="pic">
        <div class="add">
            <form action="Cart.php" method="POST">
            <button class="button"><img src="Shop.png" width="20px" height="20px"></button>
            </form>
            <div class="dropdown">
                <button class="dropbtn" onclick="drop()"><?php echo "$login_session2" ?>  <img src="Segitiga.png" width="10px" height="10px"></button>
                <div id="myDropdown" class = "dropdown-content">
                    <a href="Update Profile.php">Edit Profile</a>
                    <a href="Logout.php">Log Out</a>
                </div>
            </div>
        </div>  
    </div>
    <hr>
    <br>
    <div class="HC">
        <br><br>
        <h1>Hello Coders</h1>
        <p>Welcome to our store, please take a look for the products you might buy.</p>
    </div>
    <nav>
        <ul>
            <li>
                
                <form method="POST" action="BuyShop.php">
                <div class="nav">
                <img src="World.png" class="pic2">
                <p><b>Learning Basic Web Programming Rp.210.000.-</b></p>
                <p>Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
                <br>
                <center>
                    <input type="text" value="<?php echo "$login_session3"?>" name="user" hidden>
                    <input type="number" value="210000" name="BasicP" hidden>
                    <input type="text" value="Learning Basic Web Programming" name="BasicL" hidden>
                    <input type="submit" class="buy" name="buyB" value="Buy">
                </center>
                </form>
                </div>
            </li>
            <li>
                <div class="nav">
                    <img src="Java.png" class="pic2">
                    <form method="POST" action="BuyShop.php">
                    <p><b>Starting Programming in Java Rp.150.000.-</b></p>
                    <p>Learn Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing applications.</p>
                    <form method="POST" action="Buyshop.php">
                    <center>
                        <input type="number" value="150000" name="JavaP" hidden>
                        <input type="text" value="<?php echo "$login_session3"?>" name="user2" hidden>
                        <input type="text" value="Starting Programming in Java" name="JavaL" hidden>
                        <input type="submit" class="buy" name="buyJ" value="Buy">
                    </center>
                    </form>
                </div>
            </li>
            <li>
                <div class="nav">
                    <img src="Snake.png" class="pic2">
                    <p><b>Starting Programming in Python Rp.200.000,-</b></p>
                    <p>Learn Python - Fundamental various current industry trends: Data Sciene, Machine Learning, Infrastructure-management.</p>
                    <form method="POST" action="BuyShop.php">
                    <center>
                        <input type="number" value="200000" name="PythonP" hidden>
                        <input type="text" value="<?php echo "$login_session3"?>" name="user3" hidden>
                        <input type="text" value="Starting Programmin in Python" name="PythonL" hidden>
                        <input type="submit" class="buy" name="buyP" value="Buy">
                    </center>
                    </form>
                </div>
            </li>
        </ul>
    </nav>
    <center>
        <p>@EAD Strore</p>
    </center>
    <script>
        function drop() {
          document.getElementById("myDropdown").classList.toggle("show");
        }
        
        // Close the dropdown if the user clicks outside of it
        window.onclick = function(event) {
          if (!event.target.matches('.dropbtn')) {
            var dropdowns = document.getElementsByClassName("dropdown-content");
            var i;
            for (i = 0; i < dropdowns.length; i++) {
              var openDropdown = dropdowns[i];
              if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
              }
            }
          }
        }
        </script>
</body>
</html>