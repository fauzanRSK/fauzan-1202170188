<?php
include('login.php');

if(isset($_SESSION['login_user'])){
    header("location: HomeLogin.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
    <style>
        nav{
            margin:auto;
            text-align: center;
            height: 100%;
            width: 100%;
            font-family: arial;
        }
        nav ul {
            list-style:none;
            padding: 0 10px;
            width: 100%;
            height: 250px;
            display: inline-table;
            position: relative;

        }
        nav ul li{
            float: left;
            padding:  20px;
            position: relative;
            left:15%;
        }
        .nav{
            background-color:rgb(240,240,240);
            height: 500px;
            width: 300px;
            text-align: left;
            border: 10px;
            border-color: black;
        }
    .pic{
        width: 150px;
        height: 50px;
    }
    .pic2{
        width: 300px;
        height: 250px;
    }
    .add{
        position: relative;
        margin-left: 20%;
        margin-right: 62%;
        left: 65%;
        top: -60px;
    }
    .size{
        height: 65px;
    }
    .button{
        background-color: white;
        color: black;
        border: none;
        border-radius: 30px;
        padding: 10px;
        font-size: 16px;
        transition-duration: .5s;
        margin-top: 20px; 
    }
    .button:hover{
            color: gray;
            border: 1px crimson;
            cursor: pointer;
    }
    .HC{
        height: 200px;
        margin-left: 10%;
        margin-right: 10%;
        background-repeat:repeat-x;
        background: rgb(0,215,255);
        background: linear-gradient(90deg, rgba(0,215,255,0.35) 0%, rgba(0,236,255,0.4962359943977591) 25%, rgba(0,227,255,0.35) 50%, rgba(0,254,255,0.5) 75%, rgba(0,255,239,0.35) 100%);
    }
    .buy{
        background-color:steelblue;
        color: white;
        border: none;
        border-radius: 5px;
        padding: 10px;
        font-size: 16px;
        transition-duration: .5s;
        margin-top: 20px; 
        width: 250px;
    }
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 25%;
    }
    .modal {
        display: none; 
        position: fixed; 
        z-index: 1;
        padding-top: 100px; 
        left: 0;
        top: 0;
        width: 100%; 
        height: 100%; 
        overflow: auto; 
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0,0.4); 
    }
    .closeR {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }
    .closeL {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .closeR:hover,
    .closeR:focus {
         color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .closeL:hover,
    .closeL:focus {
         color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .ModalIn{
        padding: 5px;
        margin-top: 10px;
        margin-bottom: 10px;
        border-radius: 2px;
        width: 80%;
    }
    .login{
        background-color: blue;
            color: white;
            width: 60px;
            border: none;
            border-radius: 10px;
            padding: 7px;
            font-size: 16px;
            transition-duration: .5s;
    }
    .login:hover{
            background:white;
            color: black;
            border: 1px solid blue;
            cursor: pointer;
        }
    .close{
        background-color: grey;
            color: white;
            width: 60px;
            border: none;
            border-radius: 10px;
            padding: 7px;
            font-size: 16px;
            transition-duration: .5s;
    }
    .close:hover{
            background:white;
            color: black;
            border: 1px solid gray;
            cursor: pointer;
        }
    </style>
</head>
<body>
    <div class="size">
        <img src="EAD.png" class="pic">
        <div class="add">
            <button class="button" id="myBtnL">Login</button>
            <button class="button" id="myBtnR">Register</button>
        </div>  
    </div>
    <hr>
    <br>
    <div class="HC">
        <br><br>
        <h1>Hello Coders</h1>
        <p>Welcome to our store, please take a look for the products you might buy.</p>
    </div>
    <nav>
        <ul>
            <li>
                <div class="nav">
                <img src="World.png" class="pic2">
                <p><b>Learning Basic Web Programming Rp.210.000.-</b></p>
                <p>Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
                <br><br>
                <center>
                    <button type="button"  class="buy" id="BWP" value="Buy" onclick="buy()">Buy</button>
                </center>
                </div>
            </li>
            <li>
                <div class="nav">
                    <img src="Java.png" class="pic2">
                    <p><b>Starting Programming in Java Rp.150.000.-</b></p>
                    <p>Learn Java Language for you who want to learn the most popular Object-Oriented Programming (PBO) concepts for developing applications.</p>
                    <br>
                    <center>
                        <button type="button" class="buy" id="Java" value="Buy" onclick="buy()">Buy</button>
                    </center>
                </div>
            </li>
            <li>
                <div class="nav">
                    <img src="Snake.png" class="pic2">
                    <p><b>Starting Programming in Python Rp.200.000,-</b></p>
                    <p>Learn Python - Fundamental various current industry trends: Data Sciene, Machine Learning, Infrastructure-management.</p>
                    <br>
                    <center>
                        <button type="button" class="buy" id="Python" value="Buy" onclick="buy()">Buy</button>
                    </center>
                </div>
            </li>
        </ul>
    </nav>
    <!-- Modal -->
    <div id="myModalL" class="modal">

        <!-- MOdal Content -->
        <div class="modal-content">
            <span class="closeL">&times;</span>
            <p style="font-size:26px;"><b>Login</b></p>
            <hr>
            <form method="POST" action="Login.php">
                <label style="margin-top:10px">Email Address</label>
                <br>
                <input type="email" placeholder="Enter Emali" id="Email" name="emaill" class="ModalIn">
                <br>
                <label >Password</label>
                <br>
                <input type="password" placeholder="Password" id="pass" name="passl" class="ModalIn">
                <table style="margin-top: 20px;position: relative; left: 60%;">
                    <tr>
                        <td><button type="button" value="Close" class="close" id="closeLD">Close</button></td>
                        <td><button type="submit" value="login" class="login" name="login" id="login">Login</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModalR" class="modal">

            <!-- MOdal Content -->
            <div class="modal-content">
                <span class="closeR">&times;</span>
                <p style="font-size:26px;"><b>Register</b></p>
                <hr>
                <form method="POST" action="Registrasi.php">
                <label style="margin-top:10px">Email Address</label>
                <br>
                <input type="email" placeholder="Enter Emali" name="emailR" id="emailR" class="ModalIn">
                <br>
                <label style="margin-top:10px">Username</label>
                <br>
                <input type="text" placeholder="Enter username" id="usernameR" name="usernameR" class="ModalIn">
                <br>
                <label style="margin-top:10px">Password</label>
                <br>
                <input type="password" placeholder="Password" id="passwordR" name="passwordR" class="ModalIn">
                <br>
                <label style="margin-top:10px">Confirm Password</label>
                <br>
                <input type="password" placeholder="Confirm Password" name="conpass" id="cpR" class="ModalIn">
                <br>
                <table style="margin-top: 20px;position: relative; left: 55%;">
                    <tr>
                        <td><button type="button" value="Close" class="close" id="closeRD">Close</button></td>
                        <td><input type="submit" value="register" class="login" name="register" id="register" style="width:100px;"></td>
                    </tr>
                </table>
            </form>
            </div>
        </div>
        <center>
        <p>@EAD Strore</p>
    </center>
</body>
<script>
        var modalL = document.getElementById("myModalL");
        var modalR = document.getElementById("myModalR");
        var btnL = document.getElementById("myBtnL");
        var btnR = document.getElementById("myBtnR");
        var spanRD = document.getElementById("closeRD");
        var spanLD = document.getElementById("closeLD");
        var spanL = document.getElementsByClassName("closeL")[0];
        var spanR = document.getElementsByClassName("closeR")[0];
        btnL.onclick = function() {
          modalL.style.display = "block";
        }
        btnR.onclick = function() {
          modalR.style.display = "block";
        }
        spanL.onclick = function() {
          modalL.style.display = "none";
        }
        spanR.onclick = function() {
          modalR.style.display = "none";
        }
        spanLD.onclick = function(){
          modalL.style.display = "none";
        }
        spanRD.onclick = function(){
          modalR.style.display = "none";
        }
        window.onclick = function(event) {
          if (event.target == modalL) {
            modal.style.display = "none";
          }else if (event.target == modalR){
              modalR.style.display = "none";
          }
        }
        function buy(){
            alert("You need to Login first!!")
        }
        </script>
        
</html>